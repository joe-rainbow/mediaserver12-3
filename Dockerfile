FROM ubuntu:18.04
MAINTAINER Vinay Joseph (vinay.joseph@microfocus.com)
LABEL ACI_COMPONENT="Media Server"
EXPOSE 20000/tcp
EXPOSE 20002/tcp
EXPOSE 14000/tcp
EXPOSE 14001/tcp

#Update
RUN apt-get update
#Install Unzip
RUN apt-get install unzip
RUN apt-get install -y curl

#Unzip License Server to /opt/MicroFocus
WORKDIR /opt/MicroFocus

#Download the License Server
ADD https://storage.googleapis.com/idol-software/12.3/LicenseServer_12.3.0_LINUX_X86_64.zip /opt/MicroFocus
RUN unzip LicenseServer_12.3.0_LINUX_X86_64.zip
WORKDIR /opt/MicroFocus/LicenseServer_12.3.0_LINUX_X86_64
RUN curl -O https://storage.googleapis.com/idol-licensekey/docker/idol.dat 
RUN mv idol.dat licensekey.dat
RUN curl -O https://storage.googleapis.com/idol-configuration-files/licenseserver.cfg 
RUN ./start-licenseserver.sh


#Download the Media Server
WORKDIR /opt/MicroFocus/MediaServer
ADD https://storage.googleapis.com/idol-software/12.3/MediaServer_12.3.0_LINUX_X86_64.zip  /opt/MicroFocus/MediaServer
RUN chmod 777 MediaServer_12.3.0_LINUX_X86_64.zip 
RUN unzip MediaServer_12.3.0_LINUX_X86_64.zip 
RUN rm -f MediaServer_12.3.0_LINUX_X86_64.zip 
WORKDIR /opt/MicroFocus/MediaServer/MediaServer_12.3.0_LINUX_X86_64
RUN rm -f mediaserver.cfg
RUN curl -O https://storage.googleapis.com/idol-configuration-files/mediaserver.cfg 
RUN ./start-mediaserver.sh